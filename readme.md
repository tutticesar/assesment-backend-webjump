# WEBJUMP ASSESSMENT BACK-END PHP PLENUM

[![N|Solid](https://webjump.com.br/wp-content/uploads/2018/09/logo_webjump.png)](https://nodesource.com/products/nsolid)

## STACK
```
 - PHP 7.4
 - MariaDB .4.13
```

## PROJECT SETUP (without docker)
```
~ git clone git@bitbucket.org:tutticesar/assesment-backend-webjump.git
~ composer install
```

#### Database setup 
Change the `.env` variables, described bellow, with you own
```
APP_NAME=Webjump
APP_PORT=<port>

DB_HOST=localhost
DB_PORT=3306
DB_DATABASE=<database_name>
DB_USERNAME=<username>
DB_PASSWORD=<password>
```
#### Finally execute
```
# create tables
~ php run db:setup

# start server
~ php run serve

# if you need to clear database
~ php db:revert

the application will be runing at http:://localhost:<APP_PORT|8000>
```

## Runing with Docker
Execute `docker-compose up -d` on project's folder.
Keep the `.env` file same as bellow: 
```
DB_HOST=mysql
DB_PORT=3306
DB_DATABASE=assessment_webjump
DB_USERNAME=root
DB_PASSWORD=123456
```
**Then, execute**:
```
# access the application's container
~ docker exec -it assessment_webjump /bin/sh, access the application folder 
# acces the application's folder
~ cd app/
#run create database
~ php run db:setup
```
## Project's structure
```
\app
    \Controllers
    \Models
    \Provider
    \Repositories
    \Services
\database
\public
    \css
    \image
    index.php
\routes
\Src
    \Console
    \Container
    \Database
    \Http
    \Server
    \View
\Utils
\Viewes
```
#### \app
Is the application domain. There will be found both Product and Category controllers, Models, Services and Repositories. Also will be found the ApplicationProvider inside provider folder, used to call controllers after router's match and for Dependency Injection using `php-di/php-di` composer library.

#### \database
This folder contains the `Setup.php` class, which is used to run `setup` and `revert` commands. This commands is used to database tables creations and drop.

#### \Public
The `index.php` placed on this folder is the application's entry point. The console command `php run serve` just starts the PHP's built-in server with the command `php -S localhost:<APP_PORT|8000> pubic/` unther the hood.

#### \routers
The folder's name sepacks for itself: contains the application's rotuers.

#### \Src
This is the application's engine. Here will be the classes outside of the application domains, but used by the files in the \app folder. Here is the Request, Response, Router, console commands, the ORM prototype for the application, etc.

Best regards!