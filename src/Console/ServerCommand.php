<?php
namespace Src\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Src\Server\Server;

class ServerCommand extends Command
{
    protected $signature = 'server';
    
    protected $description = "Starts PHP's built in server";

    protected function configure()
    {
        $this->setName($this->signature)
            ->setDescription($this->description)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln("Starting server...");
        (new Server())->up();
    }
}