<?php
namespace Src\Console;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DB\Setup;

class SetupDBCommand extends Command
{
	protected $signature = 'db:setup';
    
    protected $description = "Setup db";

    protected function configure()
    {
        $this->setName($this->signature)
            ->setDescription($this->description)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
    	$output->writeln('Setting up db...');
        (new Setup)->up();
    	$output->writeln('Finished!');
        return 1;
    }
}