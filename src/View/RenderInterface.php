<?php

namespace Src\View;

interface RenderInterface
{
    public function render(string $viewPath, array $variables = []);
}
