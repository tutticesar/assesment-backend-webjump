<?php
namespace Src\View;
use \Twig\Environment;
use \Twig\Loader\FilesystemLoader;

class View implements RenderInterface
{
    public $twig;
    
    private $viewsPath = __DIR__ . '/../../views';
    
    public function __construct()
    {
        $loader = new FilesystemLoader($this->viewsPath);
        $this->twig = new Environment($loader, ['cache' => false]);
    }

    public function render(string $viewPath, array $variables = [])
    {
        return $this->twig->render($viewPath, $variables);
    }
}