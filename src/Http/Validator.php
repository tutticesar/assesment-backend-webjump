<?php
namespace Src\Http;

class Validator {

    protected function validate(array $request, array $rules, array $msgs = array(), $translateInput = array())
    {

      $msgs = $this->messages($msgs);

      $rules = $this->mapRules($request, $rules);

      $return = (object) array('errors'=>array(), 'passes'=> true);

      foreach ($rules as $key => $pipeRules) {

        $arrRules = explode('|', $pipeRules);

        foreach ($arrRules  as $rule) {
          if ($rule) {

            if ($this->{$rule}($key, $request)) {
              $key = (array_key_exists($key, $translateInput)) ? $translateInput[$key] : $key;
              $return->errors[$key][] = $msgs[$rule];
            }

          }
        }

      }

      if(!empty($return->errors)) $return->passes = false;

      return $return;
    }

    private function mapRules(array $request, array $rules) {
      return array_map(function($value) use ($request) {
        if(is_object($value)) return $value($request);
        return $value;
      }, $rules);
    }

    private function messages(array $custom = array()) {
      return array_merge(array(
        'required'  => 'Is required',
      ), $custom);
    }

    private function required($key, $request)
    {
      if(!array_key_exists($key, $request) || $request[$key] == "" || $request[$key] == null)
        return true;
      return false;
    }

}
?>