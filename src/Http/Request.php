<?php
namespace Src\Http;

class Request
{
    protected $request;

    protected $uri;

    protected $method;

    protected $headers = [];

    public function __construct()
    {
        $this->request = array_merge($_GET, $_POST);
        $this->headers = $this->getAllHeaders();
        $this->uri = explode('?', $_SERVER['REQUEST_URI'], 2)[0];
        $this->method = strtolower($_SERVER['REQUEST_METHOD']);
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function isAjax()
    {
        $xRequested = strtolower($this->header('X-REQUESTED-WITH'));
        $httXRequested = strtolower($this->header('HTTP-X-REQUESTED-WITH'));

        if ($xRequested || $httXRequested) {
            return  $xRequested == 'xmlhttprequest' || $httXRequested == 'xmlhttprequest';
        }

        return false;
    }

    public function header($param)
    {
      $val = null;
  
      foreach ($this->headers as $key => $value) {
        if (strtolower($param) == $key || strtoupper($param) == $key )
          $val = $value;
      }
      return $val;
    }
    
    public function get(string $key)
    {
        if(array_key_exists($key, $this->request))
            return $this->request[$key];
        return null;
    }

    public function all()
    {
        return $this->request;
    }

    public function getAllHeaders() : array
    {
        $headers = [];
        foreach($_SERVER as $key => $value) {

            if (substr($key, 0, 5) !== 'HTTP_') {
                continue;
            }

            $key = substr($key, 5);
            $key = str_replace('_', ' ', strtolower($key));
            
            $header = str_replace(' ', '-', ucwords($key));
            $headers[strtolower($header)] = $value;
        }
        return $headers;
    }
}