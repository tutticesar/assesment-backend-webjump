<?php
namespace Src\Http;

class Response
{
	public function redirect($path)
	{
		header('Location: '.$path);
	}
}