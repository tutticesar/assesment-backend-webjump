<?php
namespace Src\Http;
use Src\Http\Request;
use \App\Provider\ApplicationProvider;

class Dispacher
{

    private $container;

    public function __construct()
    {
        $this->container = (new ApplicationProvider());
    }

    public function dispach(array $routes)
    {
        $request = new Request();
        $uri = $request->getUri();
        $method = $request->getMethod();

        foreach ($routes[ $method ] as $route) {
            if ($uri === $route['path']) {
                
                if (is_callable($route['callback'])) {
                    
                    $this->dispachCallable($route['callback']);

                } else if (is_string($route['callback'])) {

                    if (!!strpos($route['callback'], '@') !== false) {
                        $arr = explode('@', $route['callback']);
                        $ctrl = "\App\Controllers\\". reset($arr);
                        $method = end($arr);
                        $this->dispachToController($ctrl, $method);
                    }
                }
            }
        }
    }

    protected function dispachCallable(\Closure $callback)
    {
        $callback(new Request());
    }

    protected function dispachToController(string $controller, string $method)
    {
        if ($this->isValidCall($controller, $method)) {
            $this->container->call($controller, $method);
        }
    }

    protected function isValidCall(string $controller, string $method)
    {
        $reflectionCtrl = new \ReflectionClass($controller);
        return $reflectionCtrl->isInstantiable() &&
            $reflectionCtrl->hasMethod($method)
        ;
    }

}