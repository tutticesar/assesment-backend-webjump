<?php
namespace Src\Http;

use Src\Http\Dispacher;
use App\Application;

class Router
{
    public $routes = [
        'get' => [],
        'post' => [],
        'put' => [],
        'delete' => []
    ];

    public function get(string $path, $callback)
    {
        array_push(
            $this->routes['get'],
            compact('path', 'callback')
        );
    }
    
    public function post(string $path, $callback)
    {
        array_push(
            $this->routes['post'],
            compact('path', 'callback')
        );
    }
    
    public function put(string $path, $callback)
    {
        array_push(
            $this->routes['put'],
            compact('path', 'callback')
        );
    }
    
    public function delete(string $path, $callback)
    {
        array_push(
            $this->routes['put'],
            compact('delete', 'callback')
        );
    }

    public function run()
    {
        (new Dispacher())->dispach($this->routes);
    }
}