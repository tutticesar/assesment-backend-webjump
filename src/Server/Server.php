<?php
namespace Src\Server;

class Server
{
    public function up()
    {
        $port = env('APP_PORT', "8000");
        exec("php -S localhost:{$port} -t public/");
    }
}