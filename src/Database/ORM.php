<?php
namespace Src\Database;
use Src\Database\PersistInterface;
use Src\Database\DB;

abstract class ORM implements PersistInterface
{

	protected $class;

	protected $table;

	protected $primaryKey = 'id';

	protected $db;

	public function __construct()
	{
		$this->class 	= new \ReflectionClass($this);
		$this->table 	= strtolower($this->class->getShortName());
		$this->db 		= DB::getInstance();
	}

	public function save()
	{
		$query = $this->insertQueryFromData(
			$this->getClassData()
		);

		$result = $this->db->exec($query);
		
		if ($this->db->errorCode() > 0) {
        	throw new \Exception($db->errorInfo()[2]);
	    }

	    return $this->theObject();
	}

	public function get()
	{
		$query = 'SELECT * FROM `'.$this->table.'` ORDER BY createdAT DESC';
		$stmt = $this->db->prepare($query);
		$stmt->execute();
		$result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

		if ($this->db->errorCode() > 0) {
        	throw new \Exception($db->errorInfo()[2]);
	    }

		return $this->hydrateData($result);
	}

	public function update(array $data = []) {}

	public function remove() {}

	public function findBy($name, $field) {}

	private function getClassData()
	{
		$data = [];
		
		$properties = $this->class
			->getProperties(\ReflectionProperty::IS_PRIVATE)
		;
		
		foreach ($properties as $property) {
			$name = $property->getName();
			$value = $this->{'get' . ucwords($name)}();
      		$data[$name] = $value;
	    }

	    return $data;
	}

	private function insertQueryFromData(array $data)
	{
		$queryData = [];
		foreach ($data as $key => $value) {
      		$queryData[] = '`'.$key.'` = "'.$value.'"';
	    }

	    return 'INSERT INTO `'.$this->table.'` SET '.implode(',', $queryData);
	}

	private function getPrimaryKey()
	{
		return $this->{'get' . ucwords($this->primaryKey)}();
	}

	private function hydrateData($data)
	{
		return array_map([$this, 'hydrate'], $data);
	}

	private function hydrate($data)
	{
		$class = new $this->class->name;
		foreach ($data as $name => $value) {
			if (!in_array($name, $this->ormReservedWords())) {
				$reflectionProperty = $this->class->getProperty($name);
				$reflectionProperty->setAccessible(true);
				$reflectionProperty->setValue($class, $value);
				$reflectionProperty->setAccessible(false);
			}
		}

		return $class;
	}

	public function theObject()
	{
		$data = $this->getClassData();
		return $this->hydrate( $data );
	}

	private function ormReservedWords()
	{
		return [
			'createdAt',
			'updatedAt',
			'class',
			'table',
			'primaryKey',
			'db'
		];
	}
}