<?php

namespace Src\Database;

class DB {

	protected static $instance;

	protected function __construct() {}

	public static function getInstance() {

		if(empty(self::$instance)) {
			$db_info = array(
				"db_host" => env("DB_HOST", 'locahost'),
				"db_port" => env("DB_PORT", '3306'),
				"db_user" => env("DB_USERNAME"),
				"db_pass" => env("DB_PASSWORD"),
				"db_name" => env("DB_DATABASE"),
				"db_charset" => env("DB_CHARSET", 'utf-8')
			);

			try {
				$connStr = "mysql:host=".$db_info['db_host'].';port='.$db_info['db_port'].';dbname='.$db_info['db_name'];
				self::$instance = new \PDO($connStr, $db_info['db_user'], $db_info['db_pass']);
				self::$instance->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);  
				self::$instance->setAttribute(\PDO::ATTR_EMULATE_PREPARES, false);
				self::$instance->query('SET NAMES utf8');
				self::$instance->query('SET CHARACTER SET utf8');

			} catch(\PDOException $error) {
				echo $error->getMessage();
			}

		}

		return self::$instance;
	}

	public static function setCharsetEncoding() {
		if (self::$instance == null) {
			self::connect();
		}

		self::$instance->exec(
			"SET NAMES 'utf8';
			SET character_set_connection=utf8;
			SET character_set_client=utf8;
			SET character_set_results=utf8");
	}
}
