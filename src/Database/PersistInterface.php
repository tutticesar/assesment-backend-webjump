<?php
namespace Src\Database;

interface PersistInterface
{
	public function save();

	public function get();

	public function findBy($name, $field);

	public function update(array $data = []);

	public function remove();
}