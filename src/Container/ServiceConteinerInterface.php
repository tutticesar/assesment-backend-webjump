<?php
namespace Src\Container;

interface ServiceConteinerInterface 
{

    public function call(string $controller, string $method, array $params);

    public function get(string $entity);
}