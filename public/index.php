<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include __DIR__ . '/../vendor/autoload.php';

use Symfony\Component\Dotenv\Dotenv;
use Src\Http\Router;

// register_shutdown_function('generalShutDown');

/**
 * DotEnv
 */
$dotenv = new Dotenv();
$dotenv->load(__DIR__.'/../.env');


/**
 * Routing
 */
$router = new Router();

include __DIR__ . '/../routes/web.php';

$router->run();