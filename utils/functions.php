<?php

function env(string $key, $alias = "") 
{
    return $_ENV[$key] ?? $alias;
}

function view(string $view, array $variables = []) 
{
    return app()
        ->get(Src\View\View::class)
        ->render("{$view}.twig", $variables)
    ;
}

function app()
{
    return (new App\Provider\ApplicationProvider);
}

function uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function redirect(string $path)
{
    (new Src\Http\Response)->redirect($path);
}

function generalShutDown() 
{
    echo view('errors/fatal', ['error' => error_get_last()]);
}