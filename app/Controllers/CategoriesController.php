<?php
namespace App\Controllers;
use App\Controllers\Controller;
use App\Services\CategoryService;
use Src\Http\Request;

class CategoriesController extends Controller
{

	protected $service;

	public function __construct(CategoryService $service)
	{
		$this->service = $service;
	}

    public function index()
    {
        $categories = $this->service->getAll();
        echo view('categories', compact('categories'));
    }

    public function create()
    {
        echo view('addCategory');
    }

    public function store(Request $request)
    {
        $vl = $this->validate($request->all(), [
            'name' => 'required',
            'code' => 'required'
        ]);

        if ($errors = $vl->passes) {
            $this->service->save($request->all());
            return redirect('categories');
        } else {
            echo view('addCategory', [
                'errors' => $vl->errors,
                'inputs'=>$request->all()
            ]);
        }
    }
}