<?php
namespace App\Controllers;
use App\Services\ProductService;
use App\Services\CategoryService;
use App\Controllers\Controller;
use Src\Http\Request;

class ProductController extends Controller
{
    private $service;

    private $categoryService;

    public function __construct(
        ProductService $service,
        CategoryService $categoryService
    )
    {
        $this->service = $service;
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $products = $this->service->getAll();
        echo view('products', compact('products'));
    }

    public function create()
    {
        $categories = $this->categoryService->getAll();
        echo view('addProduct', compact('categories'));
    }

    public function store(Request $request)
    {

        $vl = $this->validate($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'sku' => 'required',
            'quantity' => 'required'
        ]);

        if ($errors = $vl->passes) {
            $this->service->save($request->all());
            redirect('/products');
        } else {
            echo view('addProduct', [
                'errors' => $vl->errors,
                'inputs'=>$request->all()
            ]);
        }
    }
}