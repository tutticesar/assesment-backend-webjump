<?php
namespace App\Services;
use App\Repositories\ProductRepository;
use Src\Database\DB;
use App\Models\Product;

class ProductService
{
	private $repository;

	public function __construct(ProductRepository $repository)
	{
		$this->repository = $repository;
	}

    public function save(array $data)
    {
        $product = $this->repository->save($data);

        if (count($data['categories'])) {
            $this->repository->saveProductCategory(
                $product->getId(),
                 $data['categories']
            );
        }
        
    }

    public function getAll()
    {
        $products = $this->repository->getAll();

        foreach ($products as &$product) {
            $product->categories = $this->repository
                ->fetchCategories($product->getId());
        }

        return $products;
    }
}