<?php
namespace App\Services;

use App\Repositories\CategoryRepository;

class CategoryService
{
	private $repository;

	public function __construct(CategoryRepository $repository)
	{
		$this->repository = $repository;
	}

	public function save(array $data)
	{
		$this->repository->save($data);
	}

	public function getAll()
	{
		return $this->repository->getAll();
	}
}