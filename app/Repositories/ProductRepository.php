<?php
namespace App\Repositories;
use App\Models\Product;
use Src\Database\DB;

class ProductRepository
{
	protected $table = 'products';	

	public function save(array $data)
	{
		$product = new Product();

    	$product->setId(uuid())
    		->setName($data['name'])
    		->setSku($data['sku'])
    		->setPrice($data['price'])
    		->setDescription($data['description'])
    		->setQuantity($data['quantity'])
    		->save()
		;

		return $product;
	}

	public function getAll()
	{
		$product = new Product();
		$products = $product->get();

		return $products;
	}

	public function saveProductCategory(string $id, array $categories)
    {
        $query = $this->getCategoryProductInsertQuery($id, $categories);

        $db = DB::getInstance();
        $stmt = $db->prepare($query);

        $st = $stmt->execute();
    }

    public function clearRelationShips(string $id)
    {
        $db = DB::getInstance();

        $db->beginTransaction();

        $stmt = $db->prepare(
            "DELETE FROM category_product WHERE product_id = '{$id}'"
        );

        $stmt->execute();

        return $db;
    }

    public function fetchCategories($productId)
    {
        $db = DB::getInstance();
        $query = $this->getManyToManyRelationshipQuery($productId);
        $stmt = $db->prepare($query);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function getCategoryProductInsertQuery($productId, $categorieIds)
    {
        $query = "INSERT INTO category_product (product_id, category_id) VALUES ";

        $end = count($categorieIds) - 1;
        foreach ($categorieIds as $i => $categoryId) {
            $query .= " ('{$productId}', '{$categoryId}')" . ($i < $end ? ',' : '');
        }

        return $query;
    }

    public function getManyToManyRelationshipQuery($productId)
    {
        $query = "SELECT * FROM category WHERE id IN ( SELECT category_id 
        FROM  category_product WHERE product_id = '{$productId}' )";

        return $query;
    }

}