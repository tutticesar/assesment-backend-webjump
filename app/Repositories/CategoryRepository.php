<?php
namespace App\Repositories;
use Src\Database\DB;
use App\Models\Category;

class CategoryRepository
{

	public function save(array $data)
	{
		$category = new Category();
		
		$category->setId(uuid())
			->setName($data['name'])
			->setCode($data['code'])
		;

		return $category->save();
	}

	public function getAll()
	{
		$category = new Category();
		return $category->get();
	}
}