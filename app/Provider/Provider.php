<?php
namespace App\Provider;

use DI\ContainerBuilder;
use Src\Container\ServiceConteinerInterface;

class Provider implements ServiceConteinerInterface
{
    private $container;

    public function __construct()
    {
        $containerBuilder = new ContainerBuilder;

        if ($this->register()) {
            $containerBuilder->addDefinitions();
        }

        $this->container = $containerBuilder->build();
    }

    public function register()
    {
        return [];
    }

    public function call(string $controller, string $method, array $params = [])
    {
        return $this->container->call([$controller, $method], $params);
    }

    public function get(string $entity)
    {
        return $this->container->get($entity);
    }
}