<?php
namespace App\Provider;

use function DI\create;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

use App\Provider\Provider;

class ApplicationProvider extends Provider
{

	public function register()
	{
		return [];
	}
}