<?php
namespace DB;

use Src\Database\DB;

class Setup 
{

	private $productsTable = "CREATE TABLE IF NOT EXISTS `product` (
			`id` VARCHAR(255) NOT NULL,
			`name` VARCHAR(255) NOT NULL,
			`sku` VARCHAR(255) NOT NULL,
			`price` INT,
			`description` LONGTEXT,
			`quantity` INT,
			`createdAt` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
			`updatedAt` TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00.000000' ON UPDATE current_timestamp(),
			PRIMARY KEY (`id`)
		);";

	private $caregotiesTable = "CREATE TABLE IF NOT EXISTS `category` (
			`id` VARCHAR(255) NOT NULL,
			`name` VARCHAR(255) NOT NULL,
			`code` VARCHAR(255) NOT NULL,
			`createdAt` TIMESTAMP NOT NULL DEFAULT current_timestamp(),
			`updatedAt` TIMESTAMP ON UPDATE current_timestamp(),
			PRIMARY KEY (`id`)
		);";

	private $relationTable = "CREATE TABLE IF NOT EXISTS `category_product` (
			`product_id` VARCHAR(255) NOT NULL,
			`category_id` VARCHAR(255) NOT NULL
		);";

	public function up() 
	{


		$conn = DB::getInstance();

		try {

			$conn->beginTransaction();

			$conn->exec($this->productsTable);

			$conn->exec($this->caregotiesTable);

			$conn->exec($this->relationTable);

			$conn->commit();

			return 1;
		} catch (Exception $e) {
			echo $e->getMessage();
			$conn->rollback();
		}
	}

	public function down()
	{
		$conn = DB::getInstance();	
		
		try {

			$conn->beginTransaction();

			$conn->exec("DROP TABLE IF EXISTS `product`");

			$conn->exec("DROP TABLE IF EXISTS `category`");

			$conn->exec("DROP TABLE IF EXISTS `category_product`");

			$conn->commit();

		} catch (Exception $e) {

			echo $e->getMessage();
			$conn->rollback();	
		}	
	}
}
